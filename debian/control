Source: libscalar-listify-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 3.9.5
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libscalar-listify-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libscalar-listify-perl.git
Homepage: https://metacpan.org/release/Scalar-Listify

Package: libscalar-listify-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Multi-Arch: foreign
Description: module that produces an array/arrayref from a scalar value or array ref
 A lot of Perl code ends up with scalars having either a single scalar value
 or a reference to an array of scalar values. In order to handle the two
 conditions, one must check for what is in the scalar value before getting on
 with one's task. Ie:
 .
  $text_scalar = 'text';
 .
  $aref_scalar = [ 1.. 5 ];
 .
  print ref($text_scalar) ? (join ':', @$text_scalar) : $text_scalar;
